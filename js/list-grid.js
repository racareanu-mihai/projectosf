// Get the elements with class="product-box"
var elements = document.getElementsByClassName("product-box");

// Declare a loop variable
var i;

// List View
function listView() {
  for (i = 0; i < elements.length; i++) {
    elements[i].style.width = "90%";
  }
}

// Grid View
function gridView() {
  for (i = 0; i < elements.length; i++) {
    elements[i].style.width = "300px";
  }
}

/* Optional: Add active class to the current button (highlight it) */
var container = document.getElementById("buttons");
var btns = container.getElementsByClassName("switcher");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}