
$(document).ready(function() {
// Declare a variables - .blog is class
var dynamicContent = $(".blog");
var contentIndex = -1;
// Start function using a dinamic content and select one of them, or groups
function showNextContent() {
    ++contentIndex;
    dynamicContent.eq(contentIndex % dynamicContent.length)
        .fadeIn(2000)
        .delay(2000)
        .fadeOut(2000, showNextContent);   
}
// 2000 - Is dealy transition.
showNextContent();
});