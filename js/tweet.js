$(document).ready(function() {
// Declare a variables - .tweet - is class
var twitterContent = $(".tweet");
var twitterIndex = -1;
// Start function using a dinamic content and select one of them, or groups
function showNextTweets() {
++twitterIndex;
twitterContent.eq(twitterIndex % twitterContent.length)
    .fadeIn(2000)
    .delay(2000)
    .fadeOut(2000, showNextTweets);   
}
// 2000 - Is dealy transition.
showNextTweets();
});