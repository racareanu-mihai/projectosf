// Declare variable re for identification email address.
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
// Declare variable
function validate() {
  var $result = $("#result");
  var email = $("#email").val();
  $result.text("");
// Verified if email or not and show alert message successfully or error.
  if (validateEmail(email)) {
    alert(email + " has been registered successfully.");
  } else {
    alert(email + " is not is not a valid mail.");
  }
  return false;
}
// Binding click button id="validate"
$("#validate").bind("click", validate);